import cv2
import matplotlib.pyplot as plt
import numpy as np

# %% init
I = cv2.imread('I.jpg', cv2.IMREAD_GRAYSCALE)
J = cv2.imread('J.jpg', cv2.IMREAD_GRAYSCALE)
Idiff = cv2.absdiff(I, J)

wSize = 3
w = int(I.shape[0])
h = int(J.shape[1])
W2 = 1
dX = 1
dY = 1
u = np.zeros([w, h])
v = np.zeros([w, h])

# %% calc

for x in range(W2 + dX, w - W2 - dX):
    for y in range(W2 + dY, h - W2 - dY):
        IO = np.float32(I[x - W2:x + W2 + 1, y - W2:y + W2 + 1])
        minSum = np.float32('inf')
        for i in range(-dX, dX+1):
            for j in range(-dY, dY+1):
                JO = np.float32(J[x + i - W2:x + i + W2 + 1, y + j - W2: y + j + W2 + 1])
                sum = np.sum(np.sqrt((np.square(JO - IO))))
                if sum < minSum:
                    minSum = sum
                    u[x, y] = i
                    v[x, y] = j

# %% show

plt.gca().invert_yaxis()
plt.quiver(u, v)
plt.show()
# plt.imshow(Idiff)
# plt.show()



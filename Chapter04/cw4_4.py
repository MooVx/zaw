import cv2
import matplotlib.pyplot as plt
import numpy as np

# %% init
I = cv2.imread('I.jpg', cv2.IMREAD_GRAYSCALE)
J = cv2.imread('J.jpg', cv2.IMREAD_GRAYSCALE)
Idiff = cv2.absdiff(I, J)

wSize = 3
w = int(I.shape[0])
h = int(J.shape[1])
W2 = 1
dX = 1
dY = 1
u = np.zeros([w, h])
v = np.zeros([w, h])

scale_number = 3

# %% additional filtration

I = cv2.GaussianBlur(I, (3, 3), 1)
J = cv2.GaussianBlur(J, (3, 3), 1)


# %% optical flow function
def of(I, J, u0, v0, W2=1, dY=1, dX=1):
    w = int(I.shape[0])
    h = int(J.shape[1])
    u = np.zeros([w, h])
    v = np.zeros([w, h])
    for x in range(W2 + dX, w - W2 - dX):
        for y in range(W2 + dY, h - W2 - dY):
            IO = np.float32(I[x - W2:x + W2 + 1, y - W2:y + W2 + 1])
            minSum = np.float32('inf')
            for i in range(-dX, dX + 1):
                for j in range(-dY, dY + 1):
                    cX = int(x + u0[x, y] + i)
                    cY = int(y + v0[x, y] + j)
                    JO = np.float32(J[cX - W2:cX + W2 + 1, cY - W2: cY + W2 + 1])
                    sum = np.sum(np.sqrt((np.square(JO - IO))))
                    if sum < minSum:
                        minSum = sum
                        u[x, y] = i + u0[x, y]
                        v[x, y] = j + v0[x, y]
    return u, v


# %% pyramid function

def pyramid(im, max_scale=3):
    images = [im];
    for k in range(1, max_scale):
        images.append(cv2.resize(images[k - 1], (0, 0), fx=0.5, fy=0.5))
    return images


# %% calc

IP = pyramid(I, 3)
JP = pyramid(J, 3)
u0 = np.zeros(IP[-1].shape, np.float32)
v0 = np.zeros(JP[-1].shape, np.float32)
for s in range(scale_number - 1, -1, -1):
    print('calc scale: ', s)
    u, v = of(IP[s], JP[s], u0, v0, W2, dX, dY)
    u0 = cv2.resize(u, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
    v0 = cv2.resize(v, (0, 0), fx=2, fy=2, interpolation=cv2.INTER_NEAREST)
# %% show

plt.gca().invert_yaxis()
plt.quiver(u, v)
plt.show()
# plt.imshow(Idiff)
# plt.show()

import cv2
import numpy as np
import matplotlib.pyplot as plt

Iprev = cv2.imread('pedestrians/input/in000001.jpg', cv2.IMREAD_GRAYSCALE)

TP = 0
TN = 0
FP = 0
FN = 0
P = 0
R = 0
F1 = 0

f = open('pedestrians/temporalROI.txt','r') # otwarcie pliku
line = f.readline() # odczyt lini
roi_start, roi_end = line.split() # rozbicie lini na poszczegolne framgenty tesktu
roi_start = int(roi_start) # konwersja na int
roi_end = int(roi_end) # konwersja na int

for i in range(roi_start, roi_end):

    Icolour = cv2.imread('pedestrians/input/in%06d.jpg' % i)
    I = cv2.imread('pedestrians/input/in%06d.jpg' % i, cv2.IMREAD_GRAYSCALE)
    Iref = cv2.imread('pedestrians/groundtruth/gt%06d.png' % i, cv2.IMREAD_GRAYSCALE)
    Idiff = cv2.absdiff(I, Iprev)
    Idiff = cv2.GaussianBlur(Idiff, (5, 5), 0)
    Ibin = cv2.threshold(Idiff, 12, 255, cv2.THRESH_BINARY)[1]
    Ibin = cv2.medianBlur(Ibin, 7)
    kernelErode = np.ones((3, 3), np.uint8)
    kernelDilate = np.ones((7, 7), np.uint8)
    kernelErode2 = np.ones((7, 7), np.uint8)
    Ierode = cv2.erode(Ibin, kernelErode)
    Idilate = cv2.dilate(Ierode, kernelDilate)
    Idilate = cv2.erode(Idilate, kernelErode2)
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(Idilate)
    if stats.shape[0] > 1:
        tab = stats[1:, 4]
        pi = np.argmax(tab)
        pi = pi + 1
        cv2.rectangle(Icolour, (stats[pi, 0], stats[pi, 1]), (stats[pi, 0] + stats[pi, 2], stats[pi, 1] + stats[pi, 3]),
                      (255, 0, 0), 2)
        cv2.putText(Icolour, "%f" % stats[pi, 4], (stats[pi, 0], stats[pi, 1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                    (255, 0, 0))
        cv2.putText(Icolour, "%d" % pi, (np.int(centroids[pi, 0]), np.int(centroids[pi, 1])), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255, 0, 0))
    cv2.imshow("Idilate", Idilate)
    cv2.waitKey(1)
    cv2.imshow("Labeled", Icolour)
    cv2.waitKey(1)
    cv2.imshow("Ref", Iref)
    cv2.waitKey(1)
    Iprev = I
    TP = TP + np.sum(np.logical_and((Idilate == 255), (Iref == 255)))
    TN = TN + np.sum(np.logical_and((Idilate == 0), (Iref == 0)))
    FP = FP + np.sum(np.logical_and((Idilate == 255), (Iref == 0)))
    FN = FN + np.sum(np.logical_and((Idilate == 0), (Iref == 255)))

P = TP / (TP + FP)
R = TP / (TP + FN)
F1 = (2.0 * P * R) / (P + R)
print('F1: ', F1)

import cv2
import numpy as np
from zope.interface.interfaces import IComponentLookup

Iprev = cv2.imread('pedestrians/input/in000001.jpg',cv2.IMREAD_GRAYSCALE )

# for i in range(550,1700):
for i in range(1, 1700):

    Icolour = cv2.imread('pedestrians/input/in%06d.jpg' % i)
    I = cv2.imread('pedestrians/input/in%06d.jpg' % i,cv2.IMREAD_GRAYSCALE)
    Idiff = cv2.absdiff(I,Iprev)
    Ibin =cv2.threshold(Idiff,14,255,cv2.THRESH_BINARY)[1]
    Ibin=cv2.medianBlur(Ibin,7)
    kernelErode= np.ones((3,3),np.uint8)
    kernelDilate = np.ones((15, 15), np.uint8)
    Ierode= cv2.erode(Ibin,kernelErode)
    Idilate = cv2.dilate(Ierode, kernelDilate)
    cv2.imshow("Idilate", Idilate)
    cv2.waitKey(5)
    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(Idilate)
    # retval -- liczba znalezionych grup pikseli
    # labels -- obraz z indeksami
    # stats -- lewy skrajny punkt, gorny skrajny punkt, szerokosc, wysokosc,pole
    # centroids -- srodki ciezkosci

    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(Idilate)
    if (stats.shape[0] > 1 ):  # czy sa jakies obiekty
        tab = stats[1:,4]        # wyciecie 4 kolumny bez pierwszego elementu
        pi = np.argmax( tab )        # znalezienie indeksu najwiekszego elementu
        pi = pi + 1 # inkrementacja bo chcemy indeks w stats, a nie w tab
        # wyrysownie bbox
        cv2.rectangle(Icolour,(stats[pi,0],stats[pi,1]),(stats[pi,0]+stats[pi,2],stats[pi,1]+stats[pi,3]),(255,0,0),2)
        # wypisanie informacji o polu i numerze najwiekszego elementu
        cv2.putText(Icolour,"%f" % stats[pi,4],(stats[pi,0],stats[pi,1]),cv2.FONT_HERSHEY_SIMPLEX,0.7,(255,0,0))
        cv2.putText(Icolour,"%d" %pi,(np.int(centroids[pi,0]),np.int(centroids[pi,1])),cv2.FONT_HERSHEY_SIMPLEX,1,(255,0,0))

    cv2.imshow("Labeled", Icolour)
    cv2.waitKey(5)
    Iprev=I

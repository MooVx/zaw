import cv2

Iprev = cv2.imread('pedestrians/input/in000001.jpg',cv2.IMREAD_GRAYSCALE )

for i in range(550,1700):
    I = cv2.imread('pedestrians/input/in%06d.jpg' % i,cv2.IMREAD_GRAYSCALE)
    Idiff = cv2.absdiff(I,Iprev)
    Ibin =cv2.threshold(Idiff,60,255,cv2.THRESH_BINARY)[1]
    cv2.imshow("I", Ibin)
    cv2.waitKey(10)
    Iprev=I

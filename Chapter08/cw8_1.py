import cv2
import numpy as np
import os
import sys



trybik = cv2.imread('trybik.jpg')
trybik_gray = cv2.cvtColor(trybik, cv2.COLOR_BGR2GRAY)
trybik_gray = 255 - trybik_gray

# %% binaryze
_,trybik_mask = cv2.threshold(trybik_gray, 100, 255, cv2.THRESH_BINARY)
trybik_mask = cv2.medianBlur(trybik_mask, 5)

# %% calc moments
m = cv2.moments(trybik_mask, True)
x_c = m['m10'] / m['m00']
y_c = m['m01'] / m['m00']

# %% wyliczenie kontrow
_,contours, hierarchy = cv2.findContours(image=trybik_mask,
                                          mode=cv2.RETR_TREE,
                                          method=cv2.CHAIN_APPROX_NONE)

# %% rysowanie kontorow
trybik = cv2.drawContours(image=trybik,
                          contours=contours,
                          contourIdx=0,
                          color=(255, 0, 0))
# center of mass
cv2.circle(img=trybik,
           center=(np.int(x_c), np.int(y_c)),
           radius=1,
           color=(0, 0, 255),
           thickness=2)

# %% sobel
sobelx = cv2.Sobel(trybik_gray, cv2.CV_64F, 1, 0, ksize=5)
sobely = cv2.Sobel(trybik_gray, cv2.CV_64F, 0, 1, ksize=5)


# %% macierz gradientu
gradient = np.sqrt(sobelx ** 2 + sobely ** 2)
gradient = gradient / np.max(gradient)

# %% Rtable
orientation = np.rad2deg(np.arctan2(sobelx, sobely))
orientation += 180
orientation = np.uint16(orientation)

Rtable = [[] for i in range(360)]

for point in contours[0]:
    omega = orientation[point[0, 1], point[0, 0]]
    r = np.sqrt((point[0, 0] - x_c) ** 2 + (point[0, 1] - y_c) ** 2)
    beta = np.arctan2(point[0, 1] - y_c, point[0, 0] - x_c)
    if omega == 360:
        omega = 0
    Rtable[omega].append([r, beta])

# %% second image
trybik2 = cv2.imread('trybiki2.jpg')
trybik2_gray = cv2.cvtColor(trybik2, cv2.COLOR_BGR2GRAY)

# %% sobel
sobelx2 = cv2.Sobel(trybik2_gray, cv2.CV_64F, 1, 0, ksize=5)
sobely2 = cv2.Sobel(trybik2_gray, cv2.CV_64F, 0, 1, ksize=5)

 # %% gradient
gradient2 = np.sqrt(sobelx2 ** 2 + sobely2 ** 2)
gradient2 = gradient2 / np.max(gradient2)

orientation2 = np.rad2deg(np.arctan2(sobelx2, sobely2))
orientation2 += 180
orientation2 = np.uint16(orientation2)

accum = np.zeros(trybik2.shape[:2], dtype=np.uint16)
# accum = np.zeros([500,500], dtype=np.uint8)

for x in range(gradient2.shape[0]):
    for y in range(gradient2.shape[1]):
        if gradient2[x,y]>0.5:
            Rcol =Rtable[orientation2[x, y]]
            for r,fi in Rcol:
                x1 = int(x + r * np.sin(fi))
                y1 = int(y + r * np.cos(fi))
                if 0 <= x1 < gradient2.shape[0] and 0 <= y1 < gradient2.shape[1]:
                    accum[x1, y1] += 1



idx = np.where(accum == accum.max())
for i in range(len(idx[0])):
    cv2.circle(img=trybik2,
               center=(np.int(idx[1][i]), np.int(idx[0][i])),
               radius=2,
               color=(0, 0, 255),
               thickness=2)

accum8 = np.uint8(accum * 255.0 / accum.max())

cv2.namedWindow('accum', cv2.WINDOW_NORMAL)
cv2.resizeWindow('accum', 500, 500)
cv2.imshow('accum', accum8)
cv2.namedWindow('trybik', cv2.WINDOW_NORMAL)
cv2.resizeWindow('trybik', 500, 500)
cv2.imshow('trybik', trybik2)

cv2.waitKey(0)

cv2.destroyAllWindows()


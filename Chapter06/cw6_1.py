import cv2
import matplotlib.pyplot as plt
import numpy as np
from pyatspi import image

cap = cv2.VideoCapture('vid1_IR.avi')

while cap.isOpened():
    ret, frame = cap.read()
    if ret:
        G = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

        retval, Ibin = cv2.threshold(G, 40, 255, cv2.THRESH_BINARY)
        Ibin = cv2.medianBlur(Ibin, 3)

        Ibin = cv2.erode(Ibin, np.ones((2, 2), np.uint8))
        Ibin = cv2.dilate(Ibin, np.ones((5, 5), np.uint8))

        retval, labels, stats, centroids = cv2.connectedComponentsWithStats(image=Ibin)

        for pi in range(stats.shape[0]):
            if 100 < stats[pi, 4] < 10000 and stats[pi, 2] < stats[pi, 3]:
                for i in range(stats.shape[0]):
                    if i != pi and stats[i, 0] * 0.9 < centroids[pi, 0] < (stats[i, 0] + stats[i, 2]) * 1.1:
                        if stats[pi, 4] > stats[i, 4]:
                            centroids[i] = centroids[pi]
                        else:
                            centroids[pi] = centroids[i]
                        stats[pi, 0] = min(stats[i, 0], stats[pi, 0])
                        stats[pi, 1] = min(stats[i, 1], stats[pi, 1])
                        stats[pi, 2] = max(stats[i, 0] + stats[i, 2], stats[pi, 0] + stats[pi, 2]) - stats[pi, 0]
                        stats[pi, 3] = max(stats[i, 1] + stats[i, 3], stats[pi, 1] + stats[pi, 3]) - stats[pi, 1]
                        stats[i] = stats[pi]

            else:
                stats[pi] = 0
                centroids[pi] = 0
        for pi in range(stats.shape[0]):
            cv2.rectangle(img=frame,
                          pt1=(stats[pi, 0], stats[pi, 1]),
                          pt2=(stats[pi, 0] + stats[pi, 2], stats[pi, 1] + stats[pi, 3]),
                          color=(255, 0, 0),
                          thickness=2)
            cv2.circle(img=frame,
                       radius=2,
                       color=(0, 0, 255),
                       thickness=3,
                       center=(np.int(centroids[pi, 0]), np.int(centroids[pi, 1])))
        cv2.imshow('IR', frame)
        # cv2.imshow('Ibin', Ibin)
        cv2.waitKey(1)
    else:
        break

cap.relese()
cv2.destroyAllWindows()

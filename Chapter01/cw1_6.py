import cv2
import matplotlib.pyplot as plt
import matplotlib
# import scipy
# import scipy.misc
import numpy as np

Im = cv2.imread('mandril.jpg')
Il = cv2.imread('lena.png')

ImG = cv2.cvtColor(Im, cv2.COLOR_BGR2GRAY)
IlG = cv2.cvtColor(Il, cv2.COLOR_BGR2GRAY)

Iadd= ImG+IlG
Isub= ImG-IlG
Imul= ImG*IlG
Icob= ImG*0.3+IlG*0.7
cv2.imshow("Mandril+lena",Iadd)
cv2.imshow("Mandril-lena",Isub)
cv2.imshow("Mandril*lena",Imul)
cv2.imshow("0.3*Mandril+0.7*lena",np.uint8(Icob))
cv2.waitKey(0)
cv2.destroyAllWindows()
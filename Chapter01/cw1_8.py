import cv2
import matplotlib.pyplot as plt
import matplotlib
# import scipy
# import scipy.misc
import numpy as np

Il = cv2.imread('lena.png')
IG = cv2.cvtColor(Il, cv2.COLOR_BGR2GRAY)

IGE = cv2.equalizeHist(IG)
# [IG] -- obraz wejsciowy
# [0] -- dla obrazow w odcieniach szarosci jest tylko jeden kanal
# None -- maska (mozna liczyc histogram z wybranego fragmentu obrazu)
# [256] -- liczba przedzialow histogramu
# [0 256 ] -- zakres w ktorym liczony jest histogram

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
# clipLimit - maksymalna wysokosc slupka histogramu - wartosci powyzej rozdzielana sa pomiedzy sasiadow
# tileGridSize - rozmiar pojedycznczego bloku obrazu (metoda lokalna, dzialana rozdzielnych blokach obrazu)
I_CLAHE = clahe.apply(IG)

cv2.imshow("lena klasycznie",IGE)
cv2.imshow("lena clahe",I_CLAHE)
cv2.waitKey(0)
cv2.destroyAllWindows()

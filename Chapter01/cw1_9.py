import cv2
import matplotlib.pyplot as plt
import matplotlib
# import scipy
# import scipy.misc
import numpy as np

Il = cv2.imread('lena.png')
IG = cv2.cvtColor(Il, cv2.COLOR_BGR2GRAY)
ISobel =cv2.Sobel(IG,cv2.CV_32F, 1, 0)
IGaussianBlur =cv2.GaussianBlur(IG,(5,5),0)
ILaplacian =cv2.Laplacian(IG,cv2.CV_32F)
ImedianBlur=cv2.medianBlur(IG,5)

cv2.imshow("lena IGaussianBlur",IGaussianBlur)
cv2.waitKey(0)
cv2.imshow("lena ISobel",ISobel)
cv2.waitKey(0)
cv2.imshow("lena Laplacian",ILaplacian)
cv2.waitKey(0)
cv2.imshow("lena ImedianBlur",ImedianBlur)
cv2.waitKey(0)
cv2.destroyAllWindows()

import cv2
import matplotlib.pyplot as plt
import matplotlib
# import scipy
# import scipy.misc
import numpy as np

def hist(img):
    h=np.zeros((256,1), np.float32) # tworzy i zeruje tablice jednokolumnowa
    height, width =img.shape[:2] # shape - krotka z wymiarami - bierzemy 2 pierwsze
    for i in range(height):
        for j in range(width):
            h[img[i,j]]+=1
    return h

Il = cv2.imread('lena.png')
IG = cv2.cvtColor(Il, cv2.COLOR_BGR2GRAY)

hcv = cv2.calcHist([IG],[0],None,[256],[0,256])
# [IG] -- obraz wejsciowy
# [0] -- dla obrazow w odcieniach szarosci jest tylko jeden kanal
# None -- maska (mozna liczyc histogram z wybranego fragmentu obrazu)
# [256] -- liczba przedzialow histogramu
# [0 256 ] -- zakres w ktorym liczony jest histogram

H=hist(IG)
plt.plot(hcv)
plt.plot(H)
plt.show()
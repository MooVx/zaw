import cv2
import matplotlib.pyplot as plt
import matplotlib
import scipy
import scipy.misc

I = cv2.imread('mandril.jpg')

height, width =I.shape[:2]
scale = 1.75
# openCv
Ix2 = cv2.resize(I,(int(scale*height),int(scale*width)))
# scipy
I_2 = scipy.misc.imresize(I, 0.5)

cv2.imshow("Big Mandril",Ix2)
cv2.imshow("Small Mandril",I_2)
cv2.waitKey(0)
cv2.destroyAllWindows()
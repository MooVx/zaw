import cv2
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle

###### opencv read & show  ############

I = cv2.imread('mandril.jpg')
print(I.shape)  # rozmiary /wiersze, kolumny, glebia/
print(I.size)   # liczba bajtow
print(I.dtype)  # typ danych
cv2.imshow("Mandril",I)       # wyswietlenie
cv2.waitKey(0)                # oczekiwanie na klawisz
cv2.destroyAllWindows()

#### 1.3.1
###### matplotlib read & show  ############
I= plt.imread('mandril.jpg')
plt.figure(1)
plt.imshow(I)
plt.axis('off')
plt.title('Mandril')
###### matplotlib plot points  ############
x = [ 100, 150, 200, 250]
y = [ 50, 100, 150, 200]
plt.plot(x,y,'r.',markersize=10)
plt.show()

###### matplotlib plot path  ############
fig,ax = plt.subplots(1)
plt.figure(1)
rect = Rectangle((50,50),50,100,fill=False, ec='r');
ax.add_patch(rect)
ax.plot()
plt.show()
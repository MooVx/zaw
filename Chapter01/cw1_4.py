import cv2
import matplotlib.pyplot as plt
import matplotlib

### 1.4.1
##### Konwersje przestrzeni barw cv2
I = cv2.imread('mandril.jpg')
IG = cv2.cvtColor(I, cv2.COLOR_BGR2GRAY)
IHSV = cv2.cvtColor(I, cv2.COLOR_BGR2HSV)
cv2.imshow("MandrilGray",IG)
cv2.waitKey(0)
cv2.imshow("MandrilHSV",IHSV)
cv2.waitKey(0)
cv2.destroyAllWindows()

### 1.4.2
#### Konwersje przestrzeni barw matplotlib
def rgb2gray(I):
    return 0.299*I[:,:,2] + 0.587*I[:,:,1] + 0.114*I[:,:,0 ]
I= plt.imread('mandril.jpg')
IG= rgb2gray(I)
plt.figure(1)
plt.imshow(IG)
plt.axis('off')
plt.title('MandrilGray')
plt.gray()
plt.show()
plt.figure(2)
I_HSV = matplotlib.colors.rgb_to_hsv(I)
IH = I_HSV[:,:,0];
IS = I_HSV[:,:,1];
IV = I_HSV[:,:,2];
plt.imshow(I_HSV)
plt.axis('off')
plt.title('MandrilHSV')
# plt.gray()
plt.show()

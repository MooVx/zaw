#!/usr/bin/python
# -*- coding: utf-8 -*-

import cv2
import numpy as np
import matplotlib.pyplot as plt
import scipy.ndimage.filters as filters
import pm
import os
import sys


def get_H(img, sobel_size, gauss_size):
    Ix = cv2.Sobel(src=img, ddepth=cv2.CV_32F, dx=1, dy=0, ksize=sobel_size)
    Iy = cv2.Sobel(src=img, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=sobel_size)
    Ixy = cv2.Sobel(src=Ix, ddepth=cv2.CV_32F, dx=0, dy=1, ksize=sobel_size)
    Ix = cv2.GaussianBlur(Ix, (gauss_size, gauss_size), 0)
    Iy = cv2.GaussianBlur(Iy, (gauss_size, gauss_size), 0)
    Ixy = cv2.GaussianBlur(Ixy, (gauss_size, gauss_size), 0)
    k = 0.05
    detM = (Ix * Ix) * (Iy * Iy) - (Ixy * Ixy)
    traceM = (Ix * Ix) + (Iy * Iy)
    H = detM - k * (traceM * traceM)
    H = H / np.max(abs(H))

    return H


def find_max(image, size, threshold):  # size - rozmiar maski filtra maksymalnego
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)


def get_description(image, points_list, size):
    Y = image.shape[0]
    X = image.shape[1]

    points_list = list(
        filter(lambda pt: (pt[0] >= size) and (pt[0] < Y - size) and (pt[1] >= size) and (pt[1] < X - size),
               zip(points_list[0], points_list[1])))

    l_otoczen = []
    for i in range(len(points_list)):
        IO = image[points_list[i][0] - size:points_list[i][0] + size + 1,
             points_list[i][1] - size:points_list[i][1] + size + 1]
        I0_flatten = IO.flatten()
        I0_mean = np.mean(I0_flatten)
        I0_std = np.std(I0_flatten)
        I0_flatten = (I0_flatten - I0_mean) / I0_std
        l_otoczen.append(I0_flatten)

    wynik = list(zip(l_otoczen, points_list))
    return wynik


def compare_description(description_list_1, description_list_2, n):
    best_fit_list_dist = []
    best_fit_list_xy_1 = []
    best_fit_list_xy_2 = []
    for i in range(len(description_list_1)):
        best_dist = np.float('Inf')
        best_xy = []
        for j in range(len(description_list_2)):
            # metric - vector distance
            dist = np.sum(np.sqrt((np.square(description_list_1[i][0] - description_list_2[j][0]))))
            if dist < best_dist:
                best_dist = dist
                best_xy = description_list_2[j][1]
        best_fit_list_dist.append(best_dist)
        best_fit_list_xy_1.append(description_list_1[i][1])
        best_fit_list_xy_2.append(best_xy)

    best_list = list(zip(best_fit_list_xy_1, best_fit_list_xy_2, best_fit_list_dist))

    # sort list
    best_list.sort(key=lambda x: x[2])

    wynik = best_list[0:n]

    return wynik


DIR = os.path.dirname(sys.argv[0])
img1 = cv2.imread(DIR + "/obrazy/fontanna1.jpg", cv2.IMREAD_GRAYSCALE)
img2 = cv2.imread(DIR + "/obrazy/fontanna2.jpg", cv2.IMREAD_GRAYSCALE)

H1_img = get_H(img1, 5, 5)
H2_img = get_H(img2, 5, 5)


threshold = 0.10
size = 9
img1_maximas = find_max(H1_img, size, threshold)
img2_maximas = find_max(H2_img, size, threshold)

img_description_list_1 = get_description(img1, img1_maximas, 15)
img_description_list_2 = get_description(img2, img2_maximas, 15)
img_best_matches = compare_description(img_description_list_1, img_description_list_2, 20)


pm.plot_matches(img1, img2, img_best_matches)

plt.show()

import cv2
import numpy as np


def norm_dist(contours, img):
    xy = contours[0][:, 0, :]
    m = cv2.moments(img, True)
    x_center = m['m10'] / m['m00']
    y_center = m['m01'] / m['m00']
    xy = xy - (x_center, y_center)

    maxDist = 0.0
    for i in range(len(xy)):
        for j in range(len(xy)):
            dist = abs(xy[i, 0] - xy[j, 0]) + abs(xy[i, 1] - xy[j, 1])
            if dist > maxDist:
                maxDist = dist

    xy = xy.astype(np.float) / maxDist
    return xy,x_center,y_center


def hausdorff_one_side(a, b):
    h = 0.0
    for pA in a:
        shortest = np.Inf
        for pB in b:
            d = np.sqrt(np.sum((pA - pB) ** 2))
            if d < shortest:
                shortest = d
        if shortest > h:
            h = shortest
    return h


def hausdorff(a, b):
    return np.maximum(hausdorff_one_side(a, b), hausdorff_one_side(b, a))


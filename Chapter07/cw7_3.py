import sys
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
from functions import *

DIR = os.path.dirname(sys.argv[0])

img_ref = cv2.imread(DIR + '/ithaca_query.bmp', cv2.IMREAD_GRAYSCALE)
imgC_ref, contours_ref, hierarchy_ref = cv2.findContours(image=255 - img_ref,
                                                         mode=cv2.RETR_TREE,
                                                         method=cv2.CHAIN_APPROX_NONE)
xy_ref_norm,xc,yc = norm_dist(contours_ref, 255 - img_ref)

images = os.listdir(DIR + '/imgs/')
min_dist = np.Inf
min_name = ' '
min_fi=0.0

img = cv2.imread(DIR + '/imgs//i_ithaca.bmp', cv2.IMREAD_GRAYSCALE)
imgC, contours, hierarchy = cv2.findContours(image=255 - img,
                                             mode=cv2.RETR_TREE,
                                             method=cv2.CHAIN_APPROX_NONE)

xy_norm,xc,yc = norm_dist(contours, 255 - img)
for fi in np.linspace(0,360,10):
    xy_rot=xy_norm
    xy_rot[:, 0] = xy_norm[:, 0] * np.cos(np.deg2rad(fi)) - xy_norm[:, 1] * np.sin(np.deg2rad(fi));
    xy_rot[:, 1] = xy_norm[:, 0] * np.sin(np.deg2rad(fi)) + xy_norm[:, 1] * np.cos(np.deg2rad(fi));
    dist = hausdorff(xy_ref_norm, xy_rot)
    print('Distance = {} for {}'.format(dist, 'fi='+str(round(fi))))
    plt.scatter(xy_rot[:, 0], xy_rot[:, 1])
    plt.show()
    if dist < min_dist:
        min_dist = dist
        min_name = 'fi='+str(round(fi))
        min_fi=fi


print('Minimum distance = {} for {}'.format(min_dist, min_name))


imgC = cv2.drawContours(imgC / 2, contours, 0, (255, 0, 0))
xy,xc,yc = norm_dist(contours, 255 - img)
xy_rot=xy
xy_rot[:, 0] = xy[:, 0] * np.cos(np.deg2rad(min_fi)) - xy[:, 1] * np.sin(np.deg2rad(min_fi));
xy_rot[:, 1] = xy[:, 0] * np.sin(np.deg2rad(min_fi)) + xy[:, 1] * np.cos(np.deg2rad(min_fi));
imgC_ref = cv2.drawContours(imgC_ref / 2, contours_ref, 0, (255, 0, 0))
xy_ref,xc,yc = norm_dist(contours_ref, 255 - img_ref)

plt.imshow(imgC_ref, cmap='gray')
plt.imshow(imgC, cmap='gray')
plt.show()
plt.scatter(xy_rot[:, 0], xy_rot[:, 1])
plt.scatter(xy_ref[:, 0], xy_ref[:, 1])
plt.show()

import sys
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
from functions import *

DIR = os.path.dirname(sys.argv[0])

img_ref = cv2.imread(DIR + '/ithaca_query_rot.bmp', cv2.IMREAD_GRAYSCALE)
imgC_ref, contours_ref, hierarchy_ref = cv2.findContours(image=255 - img_ref,
                                                         mode=cv2.RETR_TREE,
                                                         method=cv2.CHAIN_APPROX_NONE)
xy_ref_norm,xc,yc = norm_dist(contours_ref,255 - img_ref)

images = os.listdir(DIR + '/imgs/')
min_dist = np.Inf
min_name = ' '
for image_name in enumerate(images):
    img = cv2.imread(DIR + '/imgs/' + image_name[1], cv2.IMREAD_GRAYSCALE)
    imgC, contours, hierarchy = cv2.findContours(image=255 - img,
                                                 mode=cv2.RETR_TREE,
                                                 method=cv2.CHAIN_APPROX_NONE)
    xy_norm,xc,yc = norm_dist(contours,255 - img)
    dist = hausdorff(xy_ref_norm, xy_norm)
    print('Distance = {} for {}'.format(dist, image_name[1]))
    if dist < min_dist:
        min_dist = dist
        min_name = image_name[1]

print('Minimum distance = {} for {}'.format(min_dist, min_name))

img = cv2.imread(DIR + '/imgs/' + min_name, cv2.IMREAD_GRAYSCALE)
imgC, contours, hierarchy = cv2.findContours(image=255 - img,
                                             mode=cv2.RETR_TREE,
                                             method=cv2.CHAIN_APPROX_NONE)
imgC = cv2.drawContours(imgC / 2, contours, 0, (255, 0, 0))
xy,xc,yc =  norm_dist(contours,255 - img)
imgC_ref = cv2.drawContours(imgC_ref / 2, contours_ref, 0, (255, 0, 0))
xy_ref,xc,yc =  norm_dist(contours_ref,255 - img_ref)

plt.imshow(imgC_ref, cmap='gray')
plt.imshow(imgC, cmap='gray')
plt.show()
plt.scatter(xy[:, 0], xy[:, 1])
plt.scatter(xy_ref[:, 0], xy_ref[:, 1])
plt.show()
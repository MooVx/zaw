import sys
import os
import cv2
import matplotlib.pyplot as plt
import numpy as np
from functions import *

DIR = os.path.dirname(sys.argv[0])

sea_img = cv2.imread(DIR + '/Aegeansea.jpg')
hsv_sea = cv2.cvtColor(sea_img, cv2.COLOR_BGR2HSV)

lower = np.array([0, 30, 0])
upper = np.array([55, 255, 255])

mask_sea = cv2.inRange(hsv_sea, lower, upper)

imgC_sea, contours_sea, hierarchy = cv2.findContours(image=mask_sea,
                                                     mode=cv2.RETR_TREE,  # first contour should be the longest
                                                     method=cv2.CHAIN_APPROX_NONE)

contours_sea = np.array([el for el in contours_sea if 15 < el.shape[0] < 3000])

image_file_name = 'c_astipalea.bmp'

island_mask = 255 - cv2.imread(DIR + '/imgs/' + image_file_name, cv2.IMREAD_GRAYSCALE)
imgC_island, island_contour, hierarchy = cv2.findContours(image=island_mask,
                                     mode=cv2.RETR_TREE,
                                     method=cv2.CHAIN_APPROX_NONE)

island_contour = np.array([el for el in island_contour])
xy_norm_island,x_center_island,y_center_island = norm_dist(island_contour, island_mask)

min_dist = np.Inf
x_center_best, y_center_best, nr_best = 0, 0, 0

for i, c in enumerate(contours_sea):
    mask = np.zeros(mask_sea.shape, np.uint8)
    mask = cv2.drawContours(image=mask,
                            contours=[c],
                            contourIdx=-1,
                            color=255,
                            thickness=cv2.FILLED)

    xy_norm_cur,x_center_cur, y_center_cur= norm_dist([c], mask)
    dist = hausdorff(xy_norm_island, xy_norm_cur)
    if dist < min_dist:
        min_dist = dist
        nr_best = i
        x_center_best, y_center_best = x_center_cur, y_center_cur

    print('{} / {}; distance = {}'.format(i+1, len(contours_sea), dist))

island_name = image_file_name.split('.')[0].split('_')[1]
cv2.putText(sea_img,  str(nr_best), (int(x_center_best), int(y_center_best)),
            cv2.FONT_HERSHEY_SIMPLEX, 2, (0, 0, 255),4,cv2.LINE_AA)

cv2.namedWindow('aegeansea_img', cv2.WINDOW_NORMAL)
cv2.resizeWindow('aegeansea_img', 600, 600)
cv2.imshow('aegeansea_img', sea_img)
cv2.imshow('island_img', island_mask)
cv2.imwrite(DIR + '/result.png', sea_img)
cv2.waitKey(0)

cv2.destroyAllWindows()
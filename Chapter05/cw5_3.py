import cv2
import matplotlib.pyplot as plt
import numpy as np



# wczytanie obrazu
img_l = cv2.imread('aloes/aloeL.jpg')
img_r = cv2.imread('aloes/aloeR.jpg')
gray_l = cv2.cvtColor(img_l, cv2.COLOR_BGR2GRAY)
gray_r = cv2.cvtColor(img_r, cv2.COLOR_BGR2GRAY)
matcherBM = cv2.StereoBM_create(numDisparities=128, blockSize=15)
depthBM = matcherBM.compute(gray_l,gray_r)
DEPTH_VISUALIZATION_SCALE = 2048
cv2.namedWindow('depthBM', cv2.WINDOW_FULLSCREEN)
cv2.imshow('depthBM', depthBM / DEPTH_VISUALIZATION_SCALE)


matcherSGBM = cv2.StereoSGBM_create(numDisparities=128, blockSize=15,minDisparity=1)
depthSGBM = matcherSGBM.compute(gray_l,gray_r)
DEPTH_VISUALIZATION_SCALE = 2048
cv2.namedWindow('depthSGBM', cv2.WINDOW_FULLSCREEN)
cv2.imshow('depthSGBM', depthSGBM / DEPTH_VISUALIZATION_SCALE)
cv2.waitKey(0)
cv2.destroyAllWindows()
import numpy as np
import cv2


def init(im_g, n_max):
    # wymiary obrazu
    w = im_g.shape[0]
    h = im_g.shape[1]
    # bufor
    m = np.zeros([w, h, n_max]).astype(np.uint8)
    # iteracja po wszystkich elementach bofora (oprocz brzegów bo losujemy z 3x3)
    for n in range(n_max):
        for x in range(1, w - 1):
            for y in range(1, h - 1):
                # losujemy z otoczenia piksela
                dx, dy = 0, 0
                while dx == 0 and dy == 0:
                    dx = np.random.randint(-1, 1)
                    dy = np.random.randint(-1, 1)
                m[x, y, n] = im_g[x + dx, y + dy].astype(np.uint8)
        cv2.imshow("M", m[:, :, n])
        cv2.waitKey(1)
    return m


def update(im, m, min_nr, r, prob):
    w = m.shape[0]
    h = m.shape[1]
    n_max = m.shape[2]
    im_back = np.zeros((w, h)).astype(np.uint8)
    # iteracja po wszystkich elementach obrazu
    for x in range(0, w):
        for y in range(0, h):
            count = 0
            n = 0
            while count < min_nr and n < n_max:
                dist = np.abs(im[x, y].astype(np.int16) - m[x, y, n].astype(np.int16))
                if dist < r:
                    count += 1
                n += 1
            if count >= min_nr:
                im_back[x, y] = 0
                if np.random.randint(0, prob) == 0:
                    m[x, y, np.random.randint(0, n_max)] = im[x, y]
                if np.random.randint(0, prob) == 0:
                    dx, dy = 0, 0
                    while dx == 0 and dy == 0:
                        dx = np.random.randint(-1, 1)
                        dy = np.random.randint(-1, 1)
                    m[x + dx, y + dy, np.random.randint(0, n_max)] = im[x, y]
            else:
                im_back[x, y] = 255
    return im_back

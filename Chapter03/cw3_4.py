import cv2
import numpy as np
import matplotlib.pyplot as plt

# glob param
N = 60
path = '../Chapter02/pedestrians/'

TP = 0
TN = 0
FP = 0
FN = 0
P = 0
R = 0
F1 = 0

alpha = 0.05

f = open(path + 'temporalROI.txt', 'r')  # otwarcie pliku
line = f.readline()  # odczyt lini
Iback = cv2.imread(path + 'input/in000001.jpg', cv2.IMREAD_GRAYSCALE)
roi_start, roi_end = line.split()  # rozbicie lini na poszczegolne framgenty tesktu
roi_start = int(roi_start)  # konwersja na int
roi_end = int(roi_end)  # konwersja na int

iN = 0
BUF = np.zeros((Iback.shape[0], Iback.shape[1], N), np.uint8)


def meanBuff(buffer):
    mean_ = np.mean(buffer, axis=2)
    mean_ = np.uint8(mean_)
    return mean_


def medianBuff(buffer):
    median_ = np.median(buffer, axis=2)
    median_ = np.uint8(median_)
    return median_


def meanAprox(IbackPrev, Icurr, alpha):
    Iback = np.uint8(alpha * np.float64(Icurr) + (1 - alpha) * np.float64(IbackPrev))
    return Iback


def medianAprox(IbackPrev, Icurr):
    mask = IbackPrev < Icurr
    Iback = IbackPrev + mask
    mask = IbackPrev > Icurr
    Iback = Iback - mask
    return Iback



for i in range(roi_start, roi_end):
    Icolour = cv2.imread(path + 'input/in%06d.jpg' % i)
    I = cv2.imread(path + 'input/in%06d.jpg' % i, cv2.IMREAD_GRAYSCALE)
    Iref = cv2.imread(path + 'groundtruth/gt%06d.png' % i, cv2.IMREAD_GRAYSCALE)
    BUF[:, :, iN] = I;
    # Iback=meanBuff(BUF)
    # Iback = medianBuff(BUF)
    # Iback = meanAprox(Iback, I, alpha)
    Iback = medianAprox(Iback, I)

    Idiff = cv2.absdiff(I, Iback)
    Idiff = cv2.GaussianBlur(Idiff, (3, 3), 0)
    Ibin = cv2.threshold(Idiff, 25, 255, cv2.THRESH_BINARY)[1]
    Ibin = cv2.medianBlur(Ibin, 5)
    kernelErode = np.ones((5, 5), np.uint8)
    kernelDilate = np.ones((5, 5), np.uint8)
    Ibin = cv2.erode(Ibin, kernelErode)
    Ibin = cv2.dilate(Ibin, kernelDilate)

    retval, labels, stats, centroids = cv2.connectedComponentsWithStats(Ibin)
    if stats.shape[0] > 1:
        tab = stats[1:, 4]
        pi = np.argmax(tab)
        pi = pi + 1
        cv2.rectangle(Icolour, (stats[pi, 0], stats[pi, 1]), (stats[pi, 0] + stats[pi, 2], stats[pi, 1] + stats[pi, 3]),
                      (255, 0, 0), 2)
        cv2.putText(Icolour, "%f" % stats[pi, 4], (stats[pi, 0], stats[pi, 1]), cv2.FONT_HERSHEY_SIMPLEX, 0.7,
                    (255, 0, 0))
        cv2.putText(Icolour, "%d" % pi, (np.int(centroids[pi, 0]), np.int(centroids[pi, 1])), cv2.FONT_HERSHEY_SIMPLEX,
                    1, (255, 0, 0))
    cv2.imshow("Iback", Iback)
    cv2.waitKey(1)
    cv2.imshow("IBin", Ibin)
    cv2.waitKey(1)
    cv2.imshow("Labeled", Icolour)
    cv2.waitKey(1)
    cv2.imshow("Ref", Iref)
    cv2.waitKey(1)
    iN += 1
    if iN >= 60:
        iN = 0
    TP = TP + np.sum(np.logical_and((Ibin == 255), (Iref == 255)))
    TN = TN + np.sum(np.logical_and((Ibin == 0), (Iref == 0)))
    FP = FP + np.sum(np.logical_and((Ibin == 255), (Iref == 0)))
    FN = FN + np.sum(np.logical_and((Ibin == 0), (Iref == 255)))

P = TP / (TP + FP)
R = TP / (TP + FN)
F1 = (2.0 * P * R) / (P + R)
print('F1: ', F1)

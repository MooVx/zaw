import cv2
import numpy as np
import os
import matplotlib
import matplotlib.pyplot as plt
import numpy.fft as fft
from matplotlib.patches import Rectangle


def hanning2D(n):
    h = np.hanning(n)
    return np.sqrt(np.outer(h, h))


def highpassFilter(size):
    rows = np.cos(np.pi * np.matrix([-0.5 + x / (size[0] - 1) for x in range(size[0])]))
    cols = np.cos(np.pi * np.matrix([-0.5 + x / (size[1] - 1) for x in range(size[1])]))
    X = np.outer(rows, cols)
    return (1.0 - X) * (2.0 - X)

def rotate(x,y,xo,yo,theta): #rotate x,y around xo,yo by theta (deg)
    xr=np.cos(np.deg2rad(theta))*(x-xo)-np.sin(np.deg2rad(theta))*(y-yo)   + xo
    yr=np.sin(np.deg2rad(theta))*(x-xo)+np.cos(np.deg2rad(theta))*(y-yo)  + yo
    return int(xr),int(yr)

# %%
plt.close('all')
DIR = os.getcwd()
wzor_img = cv2.imread(DIR + '/obrazy_Mellin/domek_r0_64.pgm', cv2.IMREAD_GRAYSCALE)
wzor_img_org: object = cv2.imread(DIR + '/obrazy_Mellin/domek_r0_64.pgm', cv2.IMREAD_GRAYSCALE)
domek_img = cv2.imread(DIR + '/obrazy_Mellin/domek_r0.pgm', cv2.IMREAD_GRAYSCALE)
wzor_img = wzor_img * hanning2D(wzor_img.shape[0])

pattern = np.zeros(domek_img.shape)
pattern[0:wzor_img.shape[0], 0:wzor_img.shape[1]] = wzor_img
offset=int((domek_img.shape[0]-wzor_img.shape[0])//2.0)
pattern_c = np.zeros(domek_img.shape)
pattern_c[offset:wzor_img.shape[0]+offset, offset:wzor_img.shape[1]+offset] = wzor_img

# %% 2D fft
fft_pattern = fft.fft2(pattern)
fft_pattern_sh=fft.fftshift(fft_pattern)
fft_domek = fft.fft2(domek_img)
fft_domek_sh=fft.fftshift(fft_domek)

# %% Amplituda i filtracja
highpass_filter = highpassFilter(pattern.shape)
fft_pattern_abs = np.abs(fft_pattern_sh)*highpass_filter
fft_domek_abs = np.abs(fft_domek_sh)*highpass_filter

# %% logPolar
R = fft_pattern.shape[0] // 2.0
M=2 * R / np.log(R)
pattern_polar = cv2.logPolar(src=fft_pattern_abs,
                                 center=(R, R),
                                 M=M,
                                 flags=cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)
domek_polar = cv2.logPolar(src=fft_domek_abs,
                              center=(R, R),
                              M=M,
                              flags=cv2.INTER_LINEAR + cv2.WARP_FILL_OUTLIERS)

# %% 2D fft
fft_pattern_polar = fft.fft2(pattern_polar)
fft_domek_polar = fft.fft2(domek_polar)

# %% phase corr
R = (np.conj(fft_pattern_polar) * fft_domek_polar) / np.abs(np.conj(fft_pattern_polar) * fft_domek_polar)
correl_norm = fft.ifft2(R)

# %% rot, scale
wsp_kata, wsp_logr = np.unravel_index(np.argmax(np.abs(correl_norm)), np.abs(correl_norm).shape)
rozmiar_logr = domek_polar.shape[1]
rozmiar_kata = domek_polar.shape[0]
if wsp_logr > rozmiar_logr // 2:  # rozmiar_logr x rozmiar_kata to rozmiar obrazu logpolar
    wykl = rozmiar_logr - wsp_logr  # powiekszenie
else:
    wykl = - wsp_logr  # pomniejszenie
A = (wsp_kata * 360.0)/rozmiar_kata
scale = np.exp(wykl / M)  # gdzie M to parametr funkcji cv2.logPolar, a wykl wyliczamy jako:
kat1 = - A  # gdzie A = (wsp_kata * 360.0 ) /rozmiar_kata
kat2 = 180 - A

srodekTrans =  ((pattern.shape[0] / 2), (pattern.shape[1]/ 2))
# srodekTrans =  (pattern.shape[0] / 2, pattern.shape[1]/ 2)
macierz_translacji_1 = cv2.getRotationMatrix2D((srodekTrans[0], srodekTrans[1]),kat1, scale)
macierz_translacji_2 = cv2.getRotationMatrix2D((srodekTrans[0], srodekTrans[1]),kat2, scale)

im_rot_scaled_1 = cv2.warpAffine(pattern_c,macierz_translacji_1, pattern.shape)
im_rot_scaled_2 = cv2.warpAffine(pattern_c,macierz_translacji_2, pattern.shape)

# %% fft2
im_rot_scaled_fft_1 = np.fft.fft2(im_rot_scaled_1)
im_rot_scaled_fft_2 = np.fft.fft2(im_rot_scaled_2)

# %% correlation
R_1 = (np.conj(im_rot_scaled_fft_1)*fft_domek) / np.abs((np.conj(im_rot_scaled_fft_1)*fft_domek))
ccor_norm_1 = np.fft.ifft2(R_1)
R_2 = (np.conj(im_rot_scaled_fft_2)*fft_domek) / np.abs((np.conj(im_rot_scaled_fft_2)*fft_domek))
ccor_norm_2 = np.fft.ifft2(R_2)
y_norm_1, x_norm_1 = np.unravel_index(np.argmax(np.abs(ccor_norm_1)), np.abs(ccor_norm_1).shape)
y_norm_2, x_norm_2 = np.unravel_index(np.argmax(np.abs(ccor_norm_2)), np.abs(ccor_norm_2).shape)

if np.max(np.abs(ccor_norm_1)) > np.max(np.abs(ccor_norm_2)):
    x_best = x_norm_1
    y_best = y_norm_1
    kat_best = kat1
    pattern_best = im_rot_scaled_1
    corr_best = ccor_norm_1
    macierz_translacji_best=macierz_translacji_1
else:
    x_best = x_norm_2
    y_best = y_norm_2
    kat_best = kat2
    pattern_best = im_rot_scaled_2
    corr_best = ccor_norm_2
    macierz_translacji_best=macierz_translacji_2

err=4
print(np.sqrt((x_best-pattern_best.shape[0]/2)**2+(y_best-pattern_best.shape[0]/2)**2))
print(np.sqrt((pattern_best.shape[0]/2)**2+(pattern_best.shape[1]/2)**2)-err)
if np.sqrt((x_best-pattern_best.shape[0]/2)**2+(y_best-pattern_best.shape[0]/2)**2)>np.sqrt((pattern_best.shape[0]/2)**2+(pattern_best.shape[1]/2)**2)-err:
    x_best=0
    y_best=pattern_best.shape[1]

# %% plots

plt.figure()
plt.imshow(wzor_img_org, 'gray')
plt.scatter(wzor_img_org.shape[0]//2, wzor_img_org.shape[0]//2, s=20, c='r')
plt.title("wzor_img_org")
plt.show()

plt.figure()
plt.imshow(domek_img, 'gray')
currentAxis = plt.gca()
xy=rotate(x_best,y_best-pattern_best.shape[1],(x_best+pattern_best.shape[0]/2),(y_best-pattern_best.shape[1]+pattern_best.shape[0]/2),kat_best)
currentAxis.add_patch(Rectangle(xy=xy,
                                width=pattern_best.shape[0],
                                height=pattern_best.shape[1],
                                angle=kat_best,
                                fill=None,
                                alpha=1,
                                color='red'))
plt.scatter(x_best+pattern_best.shape[0]//2, y_best-pattern_best.shape[1]//2, s=20, c='r')
plt.legend(['Pattern'])
plt.title('Obraz angle: '+str(kat_best))
plt.show()

# plt.figure()
# plt.imshow(im_rot_scaled_1, 'gray')
# plt.scatter(im_rot_scaled_1.shape[0]//2, im_rot_scaled_1.shape[0]//2, s=20, c='r')
# plt.title("im_rot_scaled_1")
# plt.show()
#

#
# plt.figure()
# plt.imshow(pattern_best, 'gray')
# plt.scatter(pattern_best.shape[0]//2, pattern_best.shape[0]//2, s=20, c='r')
# plt.title("pattern_best")
# plt.show()
#
# plt.figure()
# plt.imshow(np.abs(corr_best))
# plt.scatter(x_best, y_best, s=40, c='r')
# plt.title("corr_best")
# plt.show()

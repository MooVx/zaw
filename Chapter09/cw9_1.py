import cv2
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import numpy.fft as fft
from matplotlib.patches import Rectangle

plt.close('all')

DIR = os.path.dirname(sys.argv[0])

wzor_img = cv2.imread(DIR + '/obrazy_Mellin/wzor.pgm', cv2.IMREAD_GRAYSCALE)
domek_img = cv2.imread(DIR + '/obrazy_Mellin//domek_r0.pgm', cv2.IMREAD_GRAYSCALE)

pattern = np.zeros(domek_img.shape)
pattern[0:wzor_img.shape[0], 0:wzor_img.shape[1]] = wzor_img
fft_patern = fft.fft2(pattern)
fft_domek = fft.fft2(domek_img)

# %%

correl = fft.ifft2(np.conj(fft_patern) * fft_domek)

plt.figure()
plt.imshow(np.abs(correl))
y, x = np.unravel_index(np.argmax(np.abs(correl)), np.abs(correl).shape)

plt.figure()
currentAxis = plt.gca()
currentAxis.add_patch(Rectangle((x, y), wzor_img.shape[0],wzor_img.shape[1], fill=None, alpha=1,color='red'))
plt.imshow(domek_img)

plt.figure()
plt.imshow(wzor_img)


# %%

R = (np.conj(fft_patern) * fft_domek) / np.abs(np.conj(fft_patern) * fft_domek)

correl_norm = fft.ifft2(R)

plt.figure()
plt.imshow(np.abs(correl_norm))

yn, xn = np.unravel_index(np.argmax(np.abs(correl_norm)), np.abs(correl_norm).shape)

plt.figure()
currentAxis = plt.gca()
currentAxis.add_patch(Rectangle((xn, yn), wzor_img.shape[0],wzor_img.shape[1], fill=None, alpha=1,color='red'))
plt.imshow(domek_img)

# %%

dx,dy=[10,20]

macierz_translacji = np.float32([[1,0,dx],[0,1,dy]])
# gdzie dx, dy -wektor przesuniecia
obraz_przesuniety = cv2.warpAffine(pattern, macierz_translacji,(pattern.shape[1], pattern.shape[0]))

plt.figure()
plt.imshow(obraz_przesuniety)
plt.show()

fft_patern = fft.fft2(obraz_przesuniety)

R = (np.conj(fft_patern) * fft_domek) / np.abs(np.conj(fft_patern) * fft_domek)

correl_norm = fft.ifft2(R)

plt.figure()
plt.imshow(np.abs(correl_norm))

yn, xn = np.unravel_index(np.argmax(np.abs(correl_norm)), np.abs(correl_norm).shape)

plt.figure()
currentAxis = plt.gca()
currentAxis.add_patch(Rectangle((xn+dx, yn+dy), wzor_img.shape[0],wzor_img.shape[1], fill=None, alpha=1,color='red'))
plt.imshow(domek_img)
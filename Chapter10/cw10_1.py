import cv2
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import numpy.fft as fft
from matplotlib.patches import Rectangle
import scipy.ndimage.filters as filters


# %% functions def

def find_max(image, size, threshold):
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)

def h_calc(image):
    Ix = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=1,
                   dy=0)

    Iy = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=0,
                   dy=1)
    size = 3
    Ixx_blur = cv2.GaussianBlur(src=Ix ** 2, ksize=(size, size), sigmaX=0)
    Iyy_blur = cv2.GaussianBlur(src=Iy ** 2, ksize=(size, size), sigmaX=0)
    Ixy_blur = cv2.GaussianBlur(src=Ix * Iy, ksize=(size, size), sigmaX=0)
    detM = Ixx_blur * Iyy_blur - Ixy_blur ** 2
    trM = Ixx_blur + Iyy_blur
    k = 0.05
    H = detM - k * trM ** 2
    H /= H.max()
    return H

# program
plt.close('all')
DIR = os.path.dirname(sys.argv[0])

img_in = cv2.imread(DIR + '/obrazy/budynek1.jpg', cv2.IMREAD_GRAYSCALE)
img_in_2 = cv2.imread(DIR + '/obrazy/budynek2.jpg', cv2.IMREAD_GRAYSCALE)
# img_in = cv2.imread(DIR + '/obrazy/fontanna1.jpg', cv2.IMREAD_GRAYSCALE)
# img_in_2 = cv2.imread(DIR + '/obrazy/fontanna2.jpg', cv2.IMREAD_GRAYSCALE)
H1=h_calc(image=img_in)
H2=h_calc(image=img_in_2)
pt1=find_max(image=H1, size=7, threshold=0.4)
pt2=find_max(image=H2, size=7, threshold=0.4)

plt.figure()
plt.imshow(img_in,'gray')
plt.scatter(x=pt1[1],y=pt1[0],marker='*',s=10)
plt.title('img_in')
plt.show()
plt.figure()
plt.imshow(img_in_2,'gray')
plt.scatter(x=pt2[1],y=pt2[0],s=10)
plt.title('img_in_2')
plt.show()

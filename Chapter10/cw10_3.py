import cv2
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import numpy.fft as fft
from matplotlib.patches import Rectangle
import scipy.ndimage.filters as filters


# %% functions def

def pyramid(image, blur_nbr, k, sigma):
    res_shape = (blur_nbr, image.shape[0], image.shape[1])
    res_img = np.zeros(res_shape)
    fimage = np.float64(image)
    prev_img = cv2.GaussianBlur(fimage, (0, 0), sigmaX=sigma, sigmaY=sigma)
    prev_sigma = sigma
    for i in range(0, blur_nbr):
        prev_sigma = prev_sigma * k
        curr_img = cv2.GaussianBlur(fimage, (0, 0), sigmaX=prev_sigma, sigmaY=prev_sigma)
        res_img[i] = curr_img - prev_img
        # res_img[i]/=res_img[i].max()
        prev_img = curr_img
    return res_img


def find_max(image, size, threshold):
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)


def h_calc(image):
    Ix = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=1,
                   dy=0)

    Iy = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=0,
                   dy=1)
    size = 3
    Ixx_blur = cv2.GaussianBlur(src=Ix ** 2, ksize=(size, size), sigmaX=0)
    Iyy_blur = cv2.GaussianBlur(src=Iy ** 2, ksize=(size, size), sigmaX=0)
    Ixy_blur = cv2.GaussianBlur(src=Ix * Iy, ksize=(size, size), sigmaX=0)
    detM = Ixx_blur * Iyy_blur - Ixy_blur ** 2
    trM = Ixx_blur + Iyy_blur
    k = 0.05
    H = detM - k * trM ** 2
    H /= H.max()
    return H


def discribe(img, pts, env_size):
    maxX = img.shape[0]
    maxY = img.shape[1]
    surr = []
    pts = list(filter(lambda p: p[0] >= env_size and
                                p[0] < maxY - env_size and
                                p[1] >= env_size and
                                p[1] < maxX - env_size,
                      zip(pts[0], pts[1])))
    for pt in pts:
        sliceVect = img[pt[0] - env_size:pt[0] + env_size, pt[1] - env_size:pt[1] + env_size]
        sliceVect=(sliceVect-np.mean(sliceVect))/np.std(sliceVect)
        surr.append(sliceVect.flatten())
    return list(zip(sliceVect, pts))

# def compare_discribe(pts1,pts2,n):



# program
plt.close('all')
DIR = os.path.dirname(sys.argv[0])

# img_in = cv2.imread(DIR + '/obrazy/budynek1.jpg', cv2.IMREAD_GRAYSCALE)
# img_in_2 = cv2.imread(DIR + '/obrazy/budynek2.jpg', cv2.IMREAD_GRAYSCALE)
img_in = cv2.imread(DIR + '/obrazy/fontanna1.jpg', cv2.IMREAD_GRAYSCALE)
img_in_2 = cv2.imread(DIR + '/obrazy/fontanna2.jpg', cv2.IMREAD_GRAYSCALE)
H1=h_calc(image=img_in)
H2=h_calc(image=img_in_2)
pts1=find_max(image=H1, size=7, threshold=0.4)
pts2=find_max(image=H2, size=7, threshold=0.4)

pts1=discribe(img=img_in,pts=pts1,env_size=30)
pts2=discribe(img=img_in_2,pts=pts2,env_size=30)

propList=[]
for pt1 in pts1:
    tmpScore=0
    for pt2 in pts2:
        tmpScore+=np.linalg.norm(pt1[0]-pt2[0])
    propList.append([pt1[1],tmpScore])
propList.sort(key=lambda x: x[1],reverse=True)




# plt.figure()
# plt.imshow(img,'gray')
# plt.scatter(x=pt[1], y=pt[0], marker='*', s=10)
# plt.show()
# plt.figure()
# plt.imshow(img,'gray')
# plt.scatter(x=pt[1], y=pt[0], marker='*', s=10)
# plt.show()
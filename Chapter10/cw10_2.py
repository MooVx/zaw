import cv2
import numpy as np
import os
import sys
import matplotlib.pyplot as plt
import numpy.fft as fft
from matplotlib.patches import Rectangle
import scipy.ndimage.filters as filters


# %% functions def

def pyramid(image, blur_nbr, k, sigma):
    res_shape=(blur_nbr, image.shape[0], image.shape[1])
    res_img = np.zeros(res_shape)
    fimage = np.float64(image)
    prev_img = cv2.GaussianBlur(fimage,(0,0),sigmaX=sigma, sigmaY=sigma)
    prev_sigma=sigma
    for i in range(0,blur_nbr):
        prev_sigma = prev_sigma*k
        curr_img = cv2.GaussianBlur(fimage,(0,0),sigmaX=prev_sigma, sigmaY=prev_sigma)
        res_img[i]=curr_img-prev_img
        # res_img[i]/=res_img[i].max()
        prev_img=curr_img
    return res_img

def find_max(image, size, threshold):
    data_max = filters.maximum_filter(image, size)
    maxima = (image == data_max)
    diff = image > threshold
    maxima[diff == 0] = 0
    return np.nonzero(maxima)

def h_calc(image):
    Ix = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=1,
                   dy=0)

    Iy = cv2.Sobel(src=image,
                   ddepth=cv2.CV_32F,
                   dx=0,
                   dy=1)
    size = 3
    Ixx_blur = cv2.GaussianBlur(src=Ix ** 2, ksize=(size, size), sigmaX=0)
    Iyy_blur = cv2.GaussianBlur(src=Iy ** 2, ksize=(size, size), sigmaX=0)
    Ixy_blur = cv2.GaussianBlur(src=Ix * Iy, ksize=(size, size), sigmaX=0)
    detM = Ixx_blur * Iyy_blur - Ixy_blur ** 2
    trM = Ixx_blur + Iyy_blur
    k = 0.05
    H = detM - k * trM ** 2
    H /= H.max()
    return H

# program
plt.close('all')
DIR = os.path.dirname(sys.argv[0])

# img_in = cv2.imread(DIR + '/obrazy/budynek1.jpg', cv2.IMREAD_GRAYSCALE)
# img_in_2 = cv2.imread(DIR + '/obrazy/budynek2.jpg', cv2.IMREAD_GRAYSCALE)
img_in = cv2.imread(DIR + '/obrazy/fontanna1.jpg', cv2.IMREAD_GRAYSCALE)
img_in_2 = cv2.imread(DIR + '/obrazy/fontanna_pow.jpg', cv2.IMREAD_GRAYSCALE)
P1=pyramid(image=img_in,blur_nbr=5, k=1.26, sigma=1.6)
P2=pyramid(image=img_in_2,blur_nbr=10, k=1.26, sigma=1.6)
extrem1=find_max(image=P1, size=7, threshold=5)
extrem2=find_max(image=P2, size=7, threshold=5)

for i in range(5):
    x = extrem1[2]  # wspolrzedne x we wszystkich skalach
    x=x[extrem1[0]==i] # jako wspolrzedne x tylko w skali i
    y = extrem1[1]  # wspolrzedne x we wszystkich skalach
    y=y[extrem1[0]==i] # jako wspolrzedne x tylko w skali i
    plt.figure()
    plt.imshow(P1[i],'gray')
    plt.scatter(x=x,y=y,marker='*',s=10)
    plt.title('img 1 scale='+str(i))
    plt.show()
for i in range(10):
    x = extrem2[2]  # wspolrzedne x we wszystkich skalach
    x=x[extrem2[0]==i] # jako wspolrzedne x tylko w skali i
    y = extrem2[1]  # wspolrzedne x we wszystkich skalach
    y=y[extrem2[0]==i] # jako wspolrzedne x tylko w skali i
    plt.figure()
    plt.imshow(P2[i],'gray')
    plt.scatter(x=x,y=y,marker='*',s=10)
    plt.title('img 2 scale='+str(i))
    plt.show()

